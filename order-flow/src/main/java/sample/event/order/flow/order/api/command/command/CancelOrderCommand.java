package sample.event.order.flow.order.api.command.command;

import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommand;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommandType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CancelOrderCommand extends CqrsBaseCommand {
    //
    private String orderId;

    protected CancelOrderCommand(CqrsBaseCommandType type) {
        //
        super(type);
    }

    public static CancelOrderCommand newCancelOrderCommand(String orderId) {
        //
        CancelOrderCommand cancelOrderCommand = new CancelOrderCommand(CqrsBaseCommandType.Remove);
        cancelOrderCommand.setOrderId(orderId);

        return cancelOrderCommand;
    }

    public String toSting() {
        //
        return toJson();
    }
}
