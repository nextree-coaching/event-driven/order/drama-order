package sample.event.order.flow.order.api.command.rest;

import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sample.event.order.aggregate.order.api.command.command.OrderCommand;
import sample.event.order.flow.order.api.command.command.CancelOrderCommand;
import sample.event.order.flow.order.domain.logic.OrderFlowLogic;

@Slf4j
@RestController
@RequestMapping("/flow/order")
@RequiredArgsConstructor
public class OrderFlowResource implements OrderFlowFasace{
    //
    private final OrderFlowLogic orderFlowLogic;

    @PostMapping
    @Override
    public CommandResponse oderProduct(@RequestBody OrderCommand command) {
        //
        return orderFlowLogic.order(command);
    }

    @PostMapping("/cancel")
    @Override
    public CommandResponse cancleOrder(@RequestBody CancelOrderCommand command) {
        //
        return orderFlowLogic.cancelOrder(command);
    }
}
