package sample.event.order.flow.order.domain.logic;

import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naraplatform.daysboy.EventStream;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sample.event.order.aggregate.order.api.command.command.OrderCommand;
import sample.event.order.aggregate.order.domain.entity.Order;
import sample.event.order.aggregate.order.domain.entity.sdo.OrderCdo;
import sample.event.order.aggregate.order.domain.logic.OrderLogic;
import sample.event.order.aggregate.product.domain.logic.ProductViewUserLogic;
import sample.event.order.event.order.OrderCanceledEvent;
import sample.event.order.event.order.ProductSoldEvent;
import sample.event.order.flow.order.api.command.command.CancelOrderCommand;

@Service
@Transactional
@RequiredArgsConstructor
public class OrderFlowLogic {
    //
    private final OrderLogic orderLogic;
    private final ProductViewUserLogic productViewUserLogic;
    private final EventStream eventStreamService;

    public CommandResponse order(OrderCommand command) {
        //
        OrderCdo cdo = command.getOrderCdo();
        String orderId = orderLogic.registerOrder(cdo);
        productViewUserLogic.decreaseStock(cdo.getProduct().getId(), cdo.getQuantity());

        ProductSoldEvent event = new ProductSoldEvent(orderId,
                cdo.getProduct().getId(),
                cdo.getQuantity());
        eventStreamService.publishEvent(event);

        return new CommandResponse();
    }

    public CommandResponse cancelOrder(CancelOrderCommand command) {
        //
        Order order = orderLogic.findOrder(command.getOrderId());
        NameValueList nameValues = NameValueList.newInstance("canceled", "true");
        orderLogic.modifyOrder(command.getOrderId(), nameValues);
        productViewUserLogic.increaseStock(order.getProduct().getId(), order.getQuantity());

        OrderCanceledEvent event = new OrderCanceledEvent(order.getId(),
                order.getProduct().getId(),
                order.getQuantity());
        eventStreamService.publishEvent(event);

        return new CommandResponse();
    }
}
