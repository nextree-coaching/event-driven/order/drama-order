package sample.event.order.flow.order.api.command.command;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.cqrs.TraceHeader;
import io.naradrama.prologue.domain.stage.ActorKey;
import io.naradrama.prologue.util.json.JsonUtil;

import java.util.UUID;

import static sample.event.order.flow.order.api.command.command.CancelOrderCommand.newCancelOrderCommand;

public class CommandSample {
    //
    public static void main(String[] args) {
        //
        TraceHeader traceHeader = new TraceHeader();
        traceHeader.setTraceId(UUID.randomUUID().toString());
        traceHeader.setLoginUser(IdName.sample());
        traceHeader.setUserId(ActorKey.sample().getId());

        CancelOrderCommand sample = newCancelOrderCommand(UUID.randomUUID().toString());
        sample.setTraceHeader(traceHeader);
        System.out.println(JsonUtil.toJson(sample));
    }
}
