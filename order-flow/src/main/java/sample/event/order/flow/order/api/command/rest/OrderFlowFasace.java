package sample.event.order.flow.order.api.command.rest;

import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import sample.event.order.aggregate.order.api.command.command.OrderCommand;
import sample.event.order.flow.order.api.command.command.CancelOrderCommand;

public interface OrderFlowFasace {
    //
    CommandResponse oderProduct(OrderCommand command);
    CommandResponse cancleOrder(CancelOrderCommand command);
}
