package sample.event.order.flow.order.domain.logic;

import io.naraplatform.daysboy.EventStream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import sample.event.order.aggregate.order.domain.event.OrderEvent;
import sample.event.order.aggregate.order.domain.logic.OrderLogic;
import sample.event.order.event.order.ProductSoldEvent;

@Slf4j
@Component
@RequiredArgsConstructor
public class OrderHandlerLogic {
    //
    private final EventStream eventStream;
    private final OrderLogic orderLogic;

    public void onOrder(OrderEvent orderEvent) {
        //
        switch (orderEvent.getCqrsDataEventType()) {
            case Registered :
                eventStream.publishEvent(new ProductSoldEvent(orderEvent.getOrderId(),
                    orderEvent.getOrder().getProduct().getId(),
                    orderEvent.getOrder().getQuantity()));
                return;
            default:
                throw new IllegalArgumentException(orderEvent.getCqrsDataEventType()
                        + "is not allowed in OrderHandlerLogic:onOrder");
        }
    }
}
