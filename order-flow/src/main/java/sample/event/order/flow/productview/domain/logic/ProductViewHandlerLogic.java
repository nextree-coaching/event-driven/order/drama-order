package sample.event.order.flow.productview.domain.logic;

import io.naradrama.prologue.domain.IdName;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import sample.event.inventory.event.stock.ProductDeliveredEvent;
import sample.event.inventory.event.stock.ProductWarehousedEvent;
import sample.event.order.aggregate.product.domain.entity.sdo.ProductViewCdo;
import sample.event.order.aggregate.product.domain.logic.ProductViewLogic;
import sample.event.order.aggregate.product.domain.logic.ProductViewUserLogic;
import sample.event.product.event.product.ProductDomainEvent;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProductViewHandlerLogic {
    //
    private final ProductViewLogic productViewLogic;
    private final ProductViewUserLogic productViewUserLogic;

    public void onProduct(ProductDomainEvent event) {
        //
        log.debug("onProductEvent : \n{}", event.toString());
        switch (event.getEventType()) {
            case Registered:
                newProductView(event);
                break;
            case Modified:
                break;
            case Removed:
                break;
        }
    }

    private void newProductView(ProductDomainEvent event) {
        //
        String overView = String.format("%s / %s / %s / %s",
                event.getCategory(),
                event.getSize(),
                event.getMaterial(),
                event.getColor());
        ProductViewCdo cdo = new ProductViewCdo(event.getActorKey(),
                IdName.of(event.getId(), event.getName()),
                overView);

        productViewLogic.registerProductView(cdo);
    }
    public void onStock(ProductWarehousedEvent event) {
        //
        productViewUserLogic.increaseStock(event.getProductId(), event.getIncreasedQuantity());
    }

    public void onStock(ProductDeliveredEvent event) {
        //
        productViewUserLogic.decreaseStock(event.getProductId(), event.getDecreasedQuantity());
    }
}
