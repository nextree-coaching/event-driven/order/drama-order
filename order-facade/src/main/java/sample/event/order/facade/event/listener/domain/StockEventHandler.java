package sample.event.order.facade.event.listener.domain;

import io.naraplatform.daysboy.event.DaysmanEventHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import sample.event.inventory.event.stock.ProductDeliveredEvent;
import sample.event.inventory.event.stock.ProductWarehousedEvent;
import sample.event.order.flow.productview.domain.logic.ProductViewHandlerLogic;

import javax.transaction.Transactional;

@Slf4j
@Component
@RequiredArgsConstructor
@Transactional
public class StockEventHandler {
    //
    private final ProductViewHandlerLogic productViewHandlerLogic;

    @DaysmanEventHandler
    public void handle(ProductWarehousedEvent event) {
        //
        productViewHandlerLogic.onStock(event);
    }

    @DaysmanEventHandler
    public void handle(ProductDeliveredEvent event) {
        //
        productViewHandlerLogic.onStock(event);
    }
}
