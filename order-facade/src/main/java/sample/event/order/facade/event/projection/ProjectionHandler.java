/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.facade.event.projection;

import sample.event.order.aggregate.order.domain.logic.OrderLogic;
import io.naradrama.prologue.domain.cqrs.broker.StreamEventMessage;
import sample.event.order.aggregate.order.domain.event.OrderEvent;
import sample.event.order.aggregate.product.domain.event.ProductViewEvent;
import sample.event.order.aggregate.product.domain.logic.ProductViewLogic;

public class ProjectionHandler {
    private final OrderLogic orderLogic; // Autogen by nara studio
    private final ProductViewLogic productViewLogic;

    public ProjectionHandler(OrderLogic orderLogic, ProductViewLogic productViewLogic) {
        /* Autogen by nara studio */
        this.orderLogic = orderLogic;
        this.productViewLogic = productViewLogic;
    }

    public void handle(StreamEventMessage streamEventMessage) {
        /* Autogen by nara studio */
        String classFullName = streamEventMessage.getPayloadClass();
        String payload = streamEventMessage.getPayload();
        String eventName = classFullName.substring(classFullName.lastIndexOf(".") + 1);
        switch(eventName) {
            case "OrderEvent":
                OrderEvent orderEvent = OrderEvent.fromJson(payload);
                orderLogic.handleEventForProjection(orderEvent);
                break;
            case "ProductViewEvent":
                ProductViewEvent productViewEvent = ProductViewEvent.fromJson(payload);
                productViewLogic.handleEventForProjection(productViewEvent);
                break;
        }
    }
}
