package sample.event.order.facade.event.listener.domain;

import io.naraplatform.daysboy.event.DaysmanEventHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import sample.event.order.flow.productview.domain.logic.ProductViewHandlerLogic;
import sample.event.product.event.product.ProductDomainEvent;

import javax.transaction.Transactional;

@Slf4j
@Component
@RequiredArgsConstructor
@Transactional
public class ProductDomainEventHandler {
    //
    private final ProductViewHandlerLogic productViewHandlerLogic;

    @DaysmanEventHandler
    public void handle(ProductDomainEvent event) {
        //
            productViewHandlerLogic.onProduct(event);
    }
}
