package sample.event.order.event.order;

import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class OrderCanceledEvent extends CqrsDomainEvent {
    //
    private String orderId;
    private String productId;
    private int quantity;

    public OrderCanceledEvent(String orderId, String productId, int quantity) {
        //
        super();
        this.productId = productId;
        this.quantity = quantity;
    }

    public String toString() {
        //
        return toJson();
    }

    public static OrderCanceledEvent sample() {
        //
        return new OrderCanceledEvent(UUID.randomUUID().toString(), UUID.randomUUID().toString(), 10);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
