package sample.event.order.event.order;

import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class ProductSoldEvent extends CqrsDomainEvent {
    //
    private String orderId;
    private String productId;
    private int quantity;

    public ProductSoldEvent(String orderId, String productId, int quantity) {
        //
        super();
        this.productId = productId;
        this.quantity = quantity;
    }

    public String toString() {
        //
        return toJson();
    }

    public static ProductSoldEvent sample() {
        //
        return new ProductSoldEvent(UUID.randomUUID().toString(), UUID.randomUUID().toString(), 10);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
