/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.product.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsDynamicQuery;
import java.util.List;
import sample.event.order.aggregate.product.domain.entity.ProductView;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import sample.event.order.aggregate.product.store.maria.jpo.ProductViewJpo;
import io.naradrama.prologue.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import io.naradrama.prologue.domain.Offset;
import java.util.Optional;
import static java.util.Objects.nonNull;

@Getter
@Setter
@NoArgsConstructor
public class ProductViewsDynamicQuery extends CqrsDynamicQuery<List<ProductView>> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<ProductViewJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), ProductViewJpo.class);
        Offset offset = getOffset();
        TypedQuery<ProductViewJpo> query = RdbQueryBuilder.build(request, offset);
        query.setFirstResult(offset.getOffset());
        query.setMaxResults(offset.getLimit());
        List<ProductViewJpo> productViewJpos = query.getResultList();
        setQueryResult(Optional.ofNullable(productViewJpos).map(jpos -> ProductViewJpo.toDomains(jpos)).orElse(new ArrayList<>()));
        if (nonNull(getOffset()) && getOffset().isTotalCountRequested()) {
            TypedQuery<Long> countQuery = RdbQueryBuilder.buildForCount(request);
            long totalCount = countQuery.getSingleResult();
            Offset countableOffset = getOffset();
            countableOffset.setTotalCount((int) totalCount);
            setOffset(countableOffset);
        }
    }
}
