/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.product.store.maria.jpo;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Table;
import io.naradrama.prologue.store.jpa.StageEntityJpo;
import sample.event.order.aggregate.product.domain.entity.ProductView;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "PRODUCT_VIEW")
public class ProductViewJpo extends StageEntityJpo {
    //
    private String name;
    private String overView;
    private int quantity;

    public ProductViewJpo(ProductView productView) {
        //
        super(productView);
        BeanUtils.copyProperties(productView, this);
    }

    public ProductView toDomain() {
        //
        ProductView productView = new ProductView(getEntityId(), genActorKey());
        BeanUtils.copyProperties(this, productView);
        return productView;
    }

    public static List<ProductView> toDomains(List<ProductViewJpo> productViewJpos) {
        //
        return productViewJpos.stream().map(ProductViewJpo::toDomain).collect(Collectors.toList());
    }

    public String toString() {
        //
        return toJson();
    }

    public static ProductViewJpo sample() {
        //
        return new ProductViewJpo(ProductView.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
