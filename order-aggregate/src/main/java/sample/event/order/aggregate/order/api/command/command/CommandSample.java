package sample.event.order.aggregate.order.api.command.command;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.cqrs.TraceHeader;
import io.naradrama.prologue.domain.stage.ActorKey;

import java.util.UUID;

public class CommandSample {
    //
    public static void main(String[] args) {
        //
        TraceHeader traceHeader = new TraceHeader();
        traceHeader.setTraceId(UUID.randomUUID().toString());
        traceHeader.setLoginUser(IdName.sample());
        traceHeader.setUserId(ActorKey.sample().getId());

        OrderCommand sample = OrderCommand.sampleForRegister();
        sample.setTraceHeader(traceHeader);
        System.out.println(sample);
    }
}
