/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.product.domain.logic;

import io.naraplatform.daysboy.EventStream;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sample.event.order.aggregate.product.store.ProductViewStore;
import sample.event.order.aggregate.product.domain.entity.sdo.ProductViewCdo;
import sample.event.order.aggregate.product.domain.event.ProductViewEvent;
import io.naradrama.prologue.domain.NameValueList;
import java.util.List;
import java.util.stream.Collectors;
import sample.event.order.aggregate.product.domain.entity.ProductView;

import java.util.NoSuchElementException;

@Service
@Transactional
public class ProductViewLogic {
    //
    private final ProductViewStore productViewStore;
    private final EventStream eventStreamService;

    public ProductViewLogic(ProductViewStore productViewStore, EventStream eventStreamService) {
        //
        this.productViewStore = productViewStore;
        this.eventStreamService = eventStreamService;
    }

    public String registerProductView(ProductViewCdo productViewCdo) {
        //
        ProductView productView = new ProductView(productViewCdo);
        if (productViewStore.exists(productView.getEntityId())) {
            throw new IllegalArgumentException("productView already exists. " + productView.getEntityId());
        }
        productViewStore.create(productView);
        ProductViewEvent productViewEvent = ProductViewEvent.newProductViewRegisteredEvent(productView);
        eventStreamService.publishEvent(productViewEvent);
        return productView.getEntityId();
    }

    public String registerProductView(ProductViewCdo productViewCdo, NameValueList nameValueList) {
        //
        ProductView productView = ProductView.newInstance(productViewCdo, nameValueList);
        if (productViewStore.exists(productView.getEntityId())) {
            throw new IllegalArgumentException("productView already exists. " + productView.getEntityId());
        }
        productViewStore.create(productView);
        ProductViewEvent productViewEvent = ProductViewEvent.newProductViewRegisteredEvent(productView);
        eventStreamService.publishEvent(productViewEvent);
        return productView.getEntityId();
    }

    public List<String> registerProductViews(List<ProductViewCdo> productViewCdos) {
        //
        return productViewCdos.stream().map(productViewCdo -> this.registerProductView(productViewCdo)).collect(Collectors.toList());
    }

    public ProductView findProductView(String productViewId) {
        //
        ProductView productView = productViewStore.retrieve(productViewId);
        if (productView == null) {
            throw new NoSuchElementException("ProductView id: " + productViewId);
        }
        return productView;
    }

    public void modifyProductView(String productViewId, NameValueList nameValues) {
        //
        ProductView productView = findProductView(productViewId);
        productView.modify(nameValues);
        productViewStore.update(productView);
        ProductViewEvent productViewEvent = ProductViewEvent.newProductViewModifiedEvent(productViewId, nameValues);
        eventStreamService.publishEvent(productViewEvent);
    }

    public void removeProductView(String productViewId) {
        //
        ProductView productView = findProductView(productViewId);
        productViewStore.delete(productView);
        ProductViewEvent productViewEvent = ProductViewEvent.newProductViewRemovedEvent(productView.getEntityId());
        eventStreamService.publishEvent(productViewEvent);
    }

    public boolean existsProductView(String productViewId) {
        //
        return productViewStore.exists(productViewId);
    }

    public void handleEventForProjection(ProductViewEvent productViewEvent) {
        //
        switch(productViewEvent.getCqrsDataEventType()) {
            case Registered:
                productViewStore.create(productViewEvent.getProductView());
                break;
            case Modified:
                ProductView productView = productViewStore.retrieve(productViewEvent.getProductViewId());
                productView.modify(productViewEvent.getNameValues());
                productViewStore.update(productView);
                break;
            case Removed:
                productViewStore.delete(productViewEvent.getProductViewId());
                break;
        }
    }
}
