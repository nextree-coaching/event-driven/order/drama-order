/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.order.api.command.command;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommand;
import sample.event.order.aggregate.order.domain.entity.sdo.OrderCdo;
import java.util.List;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommandType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class OrderCommand extends CqrsBaseCommand {
    //
    private OrderCdo orderCdo;
    private List<OrderCdo> orderCdos;
    private boolean multiCdo;
    private String orderId;
    private NameValueList nameValues;

    protected OrderCommand(CqrsBaseCommandType type) {
        //
        super(type);
    }

    public static OrderCommand newRegisterOrderCommand(OrderCdo orderCdo) {
        //
        OrderCommand command = new OrderCommand(CqrsBaseCommandType.Register);
        command.setOrderCdo(orderCdo);
        return command;
    }

    public static OrderCommand newRegisterOrderCommand(List<OrderCdo> orderCdos) {
        //
        OrderCommand command = new OrderCommand(CqrsBaseCommandType.Register);
        command.setOrderCdos(orderCdos);
        command.setMultiCdo(true);
        return command;
    }

    public static OrderCommand newModifyOrderCommand(String orderId, NameValueList nameValues) {
        //
        OrderCommand command = new OrderCommand(CqrsBaseCommandType.Modify);
        command.setOrderId(orderId);
        command.setNameValues(nameValues);
        return command;
    }

    public static OrderCommand newRemoveOrderCommand(String orderId) {
        //
        OrderCommand command = new OrderCommand(CqrsBaseCommandType.Remove);
        command.setOrderId(orderId);
        return command;
    }

    public String toString() {
        //
        return toJson();
    }

    public static OrderCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, OrderCommand.class);
    }

    public static OrderCommand sampleForRegister() {
        //
        return newRegisterOrderCommand(OrderCdo.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(sampleForRegister());
    }
}
