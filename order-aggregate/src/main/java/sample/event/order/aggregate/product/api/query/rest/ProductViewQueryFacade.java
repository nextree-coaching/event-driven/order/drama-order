/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.product.api.query.rest;

import io.naradrama.prologue.domain.cqrs.query.QueryResponse;
import sample.event.order.aggregate.product.api.query.query.ProductViewQuery;
import sample.event.order.aggregate.product.api.query.query.ProductViewDynamicQuery;
import sample.event.order.aggregate.product.api.query.query.ProductViewsDynamicQuery;

public interface ProductViewQueryFacade {
    /* Autogen by nara studio */
    QueryResponse execute(ProductViewQuery productViewQuery);
    QueryResponse execute(ProductViewDynamicQuery productViewDynamicQuery);
    QueryResponse execute(ProductViewsDynamicQuery productViewsDynamicQuery);
}
