/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.product.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsBaseQuery;
import sample.event.order.aggregate.product.domain.entity.ProductView;
import sample.event.order.aggregate.product.store.ProductViewStore;

@Getter
@Setter
@NoArgsConstructor
public class ProductViewQuery extends CqrsBaseQuery<ProductView> {
    /* Autogen by nara studio */
    private String productViewId;

    public void execute(ProductViewStore productViewStore) {
        /* Autogen by nara studio */
        setQueryResult(productViewStore.retrieve(productViewId));
    }
}
