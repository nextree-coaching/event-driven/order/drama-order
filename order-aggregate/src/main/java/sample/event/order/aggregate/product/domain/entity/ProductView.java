package sample.event.order.aggregate.product.domain.entity;

import sample.event.order.aggregate.product.domain.entity.sdo.ProductViewCdo;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.ddd.DomainAggregate;
import io.naradrama.prologue.domain.drama.StageEntity;
import io.naradrama.prologue.domain.stage.ActorKey;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProductView  extends StageEntity implements DomainAggregate {
    //
    private String name;
    private String overView;
    private int quantity;

    public ProductView(String id, ActorKey actorKey) {
        //
        super(id, actorKey);
        quantity = 0;
    }

    public ProductView(ProductViewCdo cdo) {
        //
        super(cdo.getIdName().getId(), cdo.getActorKey());
    }

    public static ProductView newInstance(ProductViewCdo cdo, NameValueList nameValues) {
        //
        ProductView productView = new ProductView(cdo);
        productView.modifyAttributes(nameValues);

        return productView;
    }
    @Override
    protected void modifyAttributes(NameValueList nameValueList) {

    }

    @Override
    public String toJson() {
        return super.toJson();
    }

    public static ProductView sample() {
            //
        return new ProductView(ProductViewCdo.sample());
    }
    public static void main(String[] args) {
        //
        System.out.println(sample());
    }

//    public String getEntityId() {
//        //
//        return getId();
//    }

    public String getId() {
        //
        return getEntityId();
    }
}
