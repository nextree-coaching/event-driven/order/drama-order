/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.product.store;

import sample.event.order.aggregate.product.domain.entity.ProductView;

public interface ProductViewStore {
    /* Autogen by nara studio */
    void create(ProductView productView);
    ProductView retrieve(String id);
    void update(ProductView productView);
    void delete(ProductView productView);
    void delete(String id);
    boolean exists(String id);
}
