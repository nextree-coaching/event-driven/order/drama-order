/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.product.store.maria.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import sample.event.order.aggregate.product.store.maria.jpo.ProductViewJpo;

public interface ProductViewMariaRepository extends PagingAndSortingRepository<ProductViewJpo, String> {
    /* Autogen by nara studio */
}
