/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.order.api.command.rest;

import sample.event.order.aggregate.order.api.command.command.OrderCommand;
import sample.event.order.aggregate.order.domain.logic.OrderLogic;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/aggregate/order")
public class OrderResource implements OrderFacade {
    /* Autogen by nara studio */
    private final OrderLogic orderLogic;

    public OrderResource(OrderLogic orderLogic) {
        /* Autogen by nara studio */
        this.orderLogic = orderLogic;
    }

    @Override
    @PostMapping("/order/command")
    public CommandResponse executeOrder(@RequestBody OrderCommand orderCommand) {
        /* Autogen by nara studio */
        return orderLogic.routeCommand(orderCommand).getCommandResponse();
    }
}
