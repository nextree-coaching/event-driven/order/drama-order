/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.product.store.maria;

import org.springframework.stereotype.Repository;
import sample.event.order.aggregate.product.store.ProductViewStore;
import sample.event.order.aggregate.product.store.maria.repository.ProductViewMariaRepository;
import sample.event.order.aggregate.product.domain.entity.ProductView;
import sample.event.order.aggregate.product.store.maria.jpo.ProductViewJpo;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import io.naradrama.prologue.domain.Offset;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@Repository
public class ProductViewMariaStore implements ProductViewStore {
    /* Autogen by nara studio */
    private final ProductViewMariaRepository productViewMariaRepository;

    public ProductViewMariaStore(ProductViewMariaRepository productViewMariaRepository) {
        /* Autogen by nara studio */
        this.productViewMariaRepository = productViewMariaRepository;
    }

    @Override
    public void create(ProductView productView) {
        /* Autogen by nara studio */
        ProductViewJpo productViewJpo = new ProductViewJpo(productView);
        productViewMariaRepository.save(productViewJpo);
    }

    @Override
    public ProductView retrieve(String id) {
        /* Autogen by nara studio */
        Optional<ProductViewJpo> productViewJpo = productViewMariaRepository.findById(id);
        return productViewJpo.map(ProductViewJpo::toDomain).orElse(null);
    }

    @Override
    public void update(ProductView productView) {
        /* Autogen by nara studio */
        ProductViewJpo productViewJpo = new ProductViewJpo(productView);
        productViewMariaRepository.save(productViewJpo);
    }

    @Override
    public void delete(ProductView productView) {
        /* Autogen by nara studio */
        productViewMariaRepository.deleteById(productView.getEntityId());
    }

    @Override
    public void delete(String id) {
        //
        productViewMariaRepository.deleteById(id);
    }

    @Override
    public boolean exists(String id) {
        //
        return productViewMariaRepository.existsById(id);
    }

    private Pageable createPageable(Offset offset) {
        //
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }
}
