/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.order.domain.event;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEvent;
import sample.event.order.aggregate.order.domain.entity.Order;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEventType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class OrderEvent extends CqrsDataEvent {
    /* Autogen by nara studio */
    private Order order;
    private String orderId;
    private NameValueList nameValues;

    protected OrderEvent(CqrsDataEventType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static OrderEvent newOrderRegisteredEvent(Order order) {
        /* Autogen by nara studio */
        OrderEvent event = new OrderEvent(CqrsDataEventType.Registered);
        event.setOrder(order);
        return event;
    }

    public static OrderEvent newOrderModifiedEvent(String orderId, NameValueList nameValues) {
        /* Autogen by nara studio */
        OrderEvent event = new OrderEvent(CqrsDataEventType.Modified);
        event.setOrderId(orderId);
        event.setNameValues(nameValues);
        return event;
    }

    public static OrderEvent newOrderRemovedEvent(String orderId) {
        /* Autogen by nara studio */
        OrderEvent event = new OrderEvent(CqrsDataEventType.Removed);
        event.setOrderId(orderId);
        return event;
    }

    public static OrderEvent newOrderRemovedEvent(Order order) {
        /* Autogen by nara studio */
        OrderEvent event = new OrderEvent(CqrsDataEventType.Removed);
        event.setOrder(order);
        return event;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static OrderEvent fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, OrderEvent.class);
    }
}
