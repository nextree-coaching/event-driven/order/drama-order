/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.order.domain.logic;

import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.FailureMessage;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naraplatform.daysboy.EventStream;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sample.event.order.aggregate.order.api.command.command.OrderCommand;
import sample.event.order.aggregate.order.domain.entity.Order;
import sample.event.order.aggregate.order.domain.entity.sdo.OrderCdo;
import sample.event.order.aggregate.order.domain.event.OrderEvent;
import sample.event.order.aggregate.order.store.OrderXStore;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class OrderLogic {
    //
    private final OrderXStore orderStore;
    private final EventStream eventStreamService;

    public OrderLogic(OrderXStore orderStore, EventStream eventStreamService) {
        //
        this.orderStore = orderStore;
        this.eventStreamService = eventStreamService;
    }

    public OrderCommand routeCommand(OrderCommand command) {
        //
        switch(command.getCqrsBaseCommandType()) {
            case Register:
                if (command.isMultiCdo()) {
                    List<String> entityIds = this.registerOrders(command.getOrderCdos());
                    command.setCommandResponse(new CommandResponse(entityIds));
                } else {
                    String entityId = Optional.ofNullable(command.getNameValues()).filter(nameValueList -> command.getNameValues().size() != 0).map(nameValueList -> this.registerOrder(command.getOrderCdo(), nameValueList)).orElse(this.registerOrder(command.getOrderCdo()));
                    command.setCommandResponse(new CommandResponse(entityId));
                }
                break;
            case Modify:
                this.modifyOrder(command.getOrderId(), command.getNameValues());
                command.setCommandResponse(new CommandResponse(command.getOrderId()));
                break;
            case Remove:
                this.removeOrder(command.getOrderId());
                command.setCommandResponse(new CommandResponse(command.getOrderId()));
                break;
            default:
                command.setFailureMessage(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
        }
        return command;
    }

    public String registerOrder(OrderCdo orderCdo) {
        //
        Order order = new Order(orderCdo);
        if (orderStore.exists(order.getEntityId())) {
            throw new IllegalArgumentException("order already exists. " + order.getEntityId());
        }
        orderStore.create(order);
        OrderEvent orderEvent = OrderEvent.newOrderRegisteredEvent(order);
        eventStreamService.publishEvent(orderEvent);
        return order.getEntityId();
    }

    public String registerOrder(OrderCdo orderCdo, NameValueList nameValueList) {
        //
        Order order = Order.newInstance(orderCdo, nameValueList);
        if (orderStore.exists(order.getEntityId())) {
            throw new IllegalArgumentException("order already exists. " + order.getEntityId());
        }
        orderStore.create(order);
        OrderEvent orderEvent = OrderEvent.newOrderRegisteredEvent(order);
        eventStreamService.publishEvent(orderEvent);
        return order.getEntityId();
    }

    public List<String> registerOrders(List<OrderCdo> orderCdos) {
        //
        return orderCdos.stream().map(orderCdo -> this.registerOrder(orderCdo)).collect(Collectors.toList());
    }

    public Order findOrder(String orderId) {
        //
        Order order = orderStore.retrieve(orderId);
        if (order == null) {
            throw new NoSuchElementException("Order id: " + orderId);
        }
        return order;
    }

    public void modifyOrder(String orderId, NameValueList nameValues) {
        //
        Order order = findOrder(orderId);
        order.modify(nameValues);
        orderStore.update(order);
        OrderEvent orderEvent = OrderEvent.newOrderModifiedEvent(orderId, nameValues);
        eventStreamService.publishEvent(orderEvent);
    }

    public void removeOrder(String orderId) {
        //
        Order order = findOrder(orderId);
        orderStore.delete(order);
        OrderEvent orderEvent = OrderEvent.newOrderRemovedEvent(order.getEntityId());
        eventStreamService.publishEvent(orderEvent);
    }

    public boolean existsOrder(String orderId) {
        //
        return orderStore.exists(orderId);
    }

    public void handleEventForProjection(OrderEvent orderEvent) {
        //
        switch(orderEvent.getCqrsDataEventType()) {
            case Registered:
                orderStore.create(orderEvent.getOrder());
                break;
            case Modified:
                Order order = orderStore.retrieve(orderEvent.getOrderId());
                order.modify(orderEvent.getNameValues());
                orderStore.update(order);
                break;
            case Removed:
                orderStore.delete(orderEvent.getOrderId());
                break;
        }
    }
}
