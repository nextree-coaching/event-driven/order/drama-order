/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.order.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsDynamicQuery;
import java.util.List;
import sample.event.order.aggregate.order.domain.entity.Order;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import io.naradrama.prologue.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import io.naradrama.prologue.domain.Offset;
import sample.event.order.aggregate.order.store.maria.jpo.OrderXJpo;

import java.util.Optional;
import static java.util.Objects.nonNull;

@Getter
@Setter
@NoArgsConstructor
public class OrdersDynamicQuery extends CqrsDynamicQuery<List<Order>> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<OrderXJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), OrderXJpo.class);
        Offset offset = getOffset();
        TypedQuery<OrderXJpo> query = RdbQueryBuilder.build(request, offset);
        query.setFirstResult(offset.getOffset());
        query.setMaxResults(offset.getLimit());
        List<OrderXJpo> orderJpos = query.getResultList();
        setQueryResult(Optional.ofNullable(orderJpos).map(jpos -> OrderXJpo.toDomains(jpos)).orElse(new ArrayList<>()));
        if (nonNull(getOffset()) && getOffset().isTotalCountRequested()) {
            TypedQuery<Long> countQuery = RdbQueryBuilder.buildForCount(request);
            long totalCount = countQuery.getSingleResult();
            Offset countableOffset = getOffset();
            countableOffset.setTotalCount((int) totalCount);
            setOffset(countableOffset);
        }
    }
}
