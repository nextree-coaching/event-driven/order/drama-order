/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.order.store;

import sample.event.order.aggregate.order.domain.entity.Order;

public interface OrderXStore {
    /* Autogen by nara studio */
    void create(Order order);
    Order retrieve(String id);
    void update(Order order);
    void delete(Order order);
    void delete(String id);
    boolean exists(String id);
}
