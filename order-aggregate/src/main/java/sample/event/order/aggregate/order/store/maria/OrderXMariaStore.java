/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.order.store.maria;

import org.springframework.stereotype.Repository;
import sample.event.order.aggregate.order.store.OrderXStore;
import sample.event.order.aggregate.order.store.maria.repository.OrderXMariaRepository;
import sample.event.order.aggregate.order.domain.entity.Order;
import sample.event.order.aggregate.order.store.maria.jpo.OrderXJpo;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import io.naradrama.prologue.domain.Offset;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@Repository
public class OrderXMariaStore implements OrderXStore {
    /* Autogen by nara studio */
    private final OrderXMariaRepository orderXMariaRepository;

    public OrderXMariaStore(OrderXMariaRepository orderXMariaRepository) {
        /* Autogen by nara studio */
        this.orderXMariaRepository = orderXMariaRepository;
    }

    @Override
    public void create(Order order) {
        /* Autogen by nara studio */
        OrderXJpo orderXJpo = new OrderXJpo(order);
        orderXMariaRepository.save(orderXJpo);
    }

    @Override
    public Order retrieve(String id) {
        /* Autogen by nara studio */
        Optional<OrderXJpo> orderXJpo = orderXMariaRepository.findById(id);
        return orderXJpo.map(OrderXJpo::toDomain).orElse(null);
    }

    @Override
    public void update(Order order) {
        /* Autogen by nara studio */
        OrderXJpo orderXJpo = new OrderXJpo(order);
        orderXMariaRepository.save(orderXJpo);
    }

    @Override
    public void delete(Order order) {
        /* Autogen by nara studio */
        orderXMariaRepository.deleteById(order.getEntityId());
    }

    @Override
    public void delete(String id) {
        //
        orderXMariaRepository.deleteById(id);
    }

    @Override
    public boolean exists(String id) {
        //
        return orderXMariaRepository.existsById(id);
    }

    private Pageable createPageable(Offset offset) {
        //
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }
}
