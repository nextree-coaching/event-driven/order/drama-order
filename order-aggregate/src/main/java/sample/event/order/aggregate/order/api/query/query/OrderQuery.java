/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.order.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsBaseQuery;
import sample.event.order.aggregate.order.domain.entity.Order;
import sample.event.order.aggregate.order.store.OrderXStore;

@Getter
@Setter
@NoArgsConstructor
public class OrderQuery extends CqrsBaseQuery<Order> {
    /* Autogen by nara studio */
    private String orderId;

    public void execute(OrderXStore orderStore) {
        /* Autogen by nara studio */
        setQueryResult(orderStore.retrieve(orderId));
    }
}
