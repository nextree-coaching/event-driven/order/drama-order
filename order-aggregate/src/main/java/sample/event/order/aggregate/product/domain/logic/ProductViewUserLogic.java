package sample.event.order.aggregate.product.domain.logic;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sample.event.order.aggregate.product.domain.entity.ProductView;
import sample.event.order.aggregate.product.store.ProductViewStore;

import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class ProductViewUserLogic {
    //
    private final ProductViewLogic productViewLogic;
    private final ProductViewStore productViewStore;

    public int increaseStock(String productId, int quantity) {
        //
        ProductView productView = productViewLogic.findProductView(productId);
        int curQuantity = productView.getQuantity() + quantity;
        productView.setQuantity(curQuantity);
        productViewStore.update(productView);

        return curQuantity;
    }

    public int decreaseStock(String productId, int quantity) {
        //
        ProductView productView = productViewLogic.findProductView(productId);
        int curQuantity = productView.getQuantity() - quantity;
        productView.setQuantity(curQuantity);
        productViewStore.update(productView);

        return curQuantity;
    }
}
