package sample.event.order.aggregate.order.domain.entity;

import sample.event.order.aggregate.order.domain.entity.sdo.OrderCdo;
import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.ddd.DomainAggregate;
import io.naradrama.prologue.domain.drama.StageEntity;
import io.naradrama.prologue.domain.stage.ActorKey;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Order extends StageEntity implements DomainAggregate {
    //
    private IdName product;
    private int quantity;
    private boolean canceled;

    public Order(String id, ActorKey actorKey) {
        //
        super(id, actorKey);
        canceled = false;
    }

    public Order(OrderCdo cdo) {
        //
        super(cdo.getActorKey());
        this.product = cdo.getProduct();
        this.quantity = cdo.getQuantity();
    }

    public static Order newInstance(OrderCdo cdo, NameValueList nameValues) {
        //
        Order order = new Order(cdo);
        order.modifyAttributes(nameValues);

        return order;
    }

    @Override
    protected void modifyAttributes(NameValueList nameValueList) {

    }

    @Override
    public String toJson() {
        return super.toJson();
    }

    public static Order sample() {
        //
        return new Order(OrderCdo.sample());
    }

//    public String getEntityId() {
//        //
//        return getId();
//    }

    public String getId() {
        //
        return getEntityId();
    }
    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
