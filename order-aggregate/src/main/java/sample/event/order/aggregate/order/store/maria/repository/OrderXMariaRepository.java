/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.order.store.maria.repository;

import javax.persistence.QueryHint;
import org.springframework.data.repository.PagingAndSortingRepository;
import sample.event.order.aggregate.order.store.maria.jpo.OrderXJpo;

public interface OrderXMariaRepository extends PagingAndSortingRepository<OrderXJpo, String> {
    /* Autogen by nara studio */
}
