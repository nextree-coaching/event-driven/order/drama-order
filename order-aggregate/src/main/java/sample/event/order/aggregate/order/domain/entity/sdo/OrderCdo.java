package sample.event.order.aggregate.order.domain.entity.sdo;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.stage.ActorKey;
import io.naradrama.prologue.util.json.JsonSerializable;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderCdo implements JsonSerializable {
    //
    private ActorKey actorKey;
    private IdName product;
    private int quantity;

    public String toString() {
        //
        return toJson();
    }

    public static OrderCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, OrderCdo.class);
    }

    public static OrderCdo sample() {
        //
        return new OrderCdo(ActorKey.sample(),
                IdName.of(UUID.randomUUID().toString(), "dokdo belongs to korea"),
                1);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
