/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.order.api.query.rest;

import io.naradrama.prologue.domain.cqrs.query.QueryResponse;
import sample.event.order.aggregate.order.api.query.query.OrderQuery;
import sample.event.order.aggregate.order.api.query.query.OrderDynamicQuery;
import sample.event.order.aggregate.order.api.query.query.OrdersDynamicQuery;

public interface OrderQueryFacade {
    /* Autogen by nara studio */
    QueryResponse execute(OrderQuery orderQuery);
    QueryResponse execute(OrderDynamicQuery orderDynamicQuery);
    QueryResponse execute(OrdersDynamicQuery ordersDynamicQuery);
}
