/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.order.api.command.rest;

import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import sample.event.order.aggregate.order.api.command.command.OrderCommand;

public interface OrderFacade {
    /* Autogen by nara studio */
    CommandResponse executeOrder(OrderCommand orderCommand);
}
