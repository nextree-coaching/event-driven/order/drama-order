/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.order.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsDynamicQuery;
import sample.event.order.aggregate.order.domain.entity.Order;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import io.naradrama.prologue.util.query.RdbQueryBuilder;
import sample.event.order.aggregate.order.store.maria.jpo.OrderXJpo;

import javax.persistence.TypedQuery;
import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
public class OrderDynamicQuery extends CqrsDynamicQuery<Order> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<OrderXJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), OrderXJpo.class);
        TypedQuery<OrderXJpo> query = RdbQueryBuilder.build(request);
        OrderXJpo orderJpo = query.getSingleResult();
        setQueryResult(Optional.ofNullable(orderJpo).map(jpo -> jpo.toDomain()).orElse(null));
    }
}
