package sample.event.order.aggregate.product.domain.entity.sdo;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.stage.ActorKey;
import io.naradrama.prologue.util.json.JsonSerializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductViewCdo implements JsonSerializable {
    //
    private ActorKey actorKey;
    private IdName idName;
    private String overView;

    public String toString() {
        //
        return toJson();
    }

    public static ProductViewCdo sample() {
        //
        String name = "dokdo belongs to korea";
        String overView = "sneakers/240/cowhide/red";

        return new ProductViewCdo(ActorKey.sample(),
                IdName.of(UUID.randomUUID().toString(), name),
                overView);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
