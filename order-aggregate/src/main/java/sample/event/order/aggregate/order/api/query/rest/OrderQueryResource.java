/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.order.api.query.rest;

import io.naradrama.prologue.domain.cqrs.query.QueryResponse;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sample.event.order.aggregate.order.api.query.query.OrderDynamicQuery;
import sample.event.order.aggregate.order.api.query.query.OrderQuery;
import sample.event.order.aggregate.order.api.query.query.OrdersDynamicQuery;
import sample.event.order.aggregate.order.store.OrderXStore;
import sample.event.order.aggregate.order.store.maria.jpo.OrderXJpo;

import javax.persistence.EntityManager;

@RestController
@RequestMapping("/aggregate/order/order/query")
public class OrderQueryResource implements OrderQueryFacade {
    //
    private final OrderXStore orderStore;
    private final RdbQueryRequest<OrderXJpo> request;

    public OrderQueryResource(OrderXStore orderStore, EntityManager entityManager) {
        //
        this.orderStore = orderStore;
        this.request = new RdbQueryRequest<>(entityManager);
    }

    @Override
    @PostMapping("/")
    public QueryResponse execute(@RequestBody OrderQuery orderQuery) {
        //
        orderQuery.execute(orderStore);
        return orderQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-single")
    public QueryResponse execute(@RequestBody OrderDynamicQuery orderDynamicQuery) {
        //
        orderDynamicQuery.execute(request);
        return orderDynamicQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-multi")
    public QueryResponse execute(@RequestBody OrdersDynamicQuery ordersDynamicQuery) {
        //
        ordersDynamicQuery.execute(request);
        return ordersDynamicQuery.getQueryResponse();
    }
}
