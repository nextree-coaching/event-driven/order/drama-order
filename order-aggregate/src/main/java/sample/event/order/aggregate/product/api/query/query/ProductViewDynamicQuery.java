/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.product.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsDynamicQuery;
import sample.event.order.aggregate.product.domain.entity.ProductView;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import sample.event.order.aggregate.product.store.maria.jpo.ProductViewJpo;
import io.naradrama.prologue.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;
import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
public class ProductViewDynamicQuery extends CqrsDynamicQuery<ProductView> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<ProductViewJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), ProductViewJpo.class);
        TypedQuery<ProductViewJpo> query = RdbQueryBuilder.build(request);
        ProductViewJpo productViewJpo = query.getSingleResult();
        setQueryResult(Optional.ofNullable(productViewJpo).map(jpo -> jpo.toDomain()).orElse(null));
    }
}
