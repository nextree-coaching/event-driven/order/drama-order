/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.order.store.maria.jpo;

import io.naradrama.prologue.domain.IdName;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Table;
import io.naradrama.prologue.store.jpa.StageEntityJpo;
import sample.event.order.aggregate.order.domain.entity.Order;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "ORDERX")
public class OrderXJpo extends StageEntityJpo {
    //
    private String productId;
    private String productName;
    private int quantity;
    private boolean canceled;

    public OrderXJpo(Order order) {
        //
        super(order);
        BeanUtils.copyProperties(order, this);
        this.productId = order.getProduct().getId();
        this.productName = order.getProduct().getName();
    }

    public Order toDomain() {
        //
        Order order = new Order(getEntityId(), genActorKey());
        BeanUtils.copyProperties(this, order);
        IdName idName = new IdName();
        idName.setId(productId);
        idName.setName(productName);
        order.setProduct(idName);
        return order;
    }

    public static List<Order> toDomains(List<OrderXJpo> orderXJpos) {
        //
        return orderXJpos.stream().map(OrderXJpo::toDomain).collect(Collectors.toList());
    }

    public String toString() {
        //
        return toJson();
    }

    public static OrderXJpo sample() {
        //
        return new OrderXJpo(Order.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
