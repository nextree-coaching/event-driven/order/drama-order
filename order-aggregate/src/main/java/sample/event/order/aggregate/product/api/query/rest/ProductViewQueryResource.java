/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.product.api.query.rest;

import sample.event.order.aggregate.product.api.query.query.ProductViewDynamicQuery;
import sample.event.order.aggregate.product.api.query.query.ProductViewQuery;
import sample.event.order.aggregate.product.api.query.query.ProductViewsDynamicQuery;
import sample.event.order.aggregate.product.store.ProductViewStore;
import sample.event.order.aggregate.product.store.maria.jpo.ProductViewJpo;
import io.naradrama.prologue.domain.cqrs.query.QueryResponse;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;

@RestController
@RequestMapping("/aggregate/product/product-view/query")
public class ProductViewQueryResource implements ProductViewQueryFacade {
    //
    private final ProductViewStore productViewStore;
    private final RdbQueryRequest<ProductViewJpo> request;

    public ProductViewQueryResource(ProductViewStore productViewStore, EntityManager entityManager) {
        //
        this.productViewStore = productViewStore;
        this.request = new RdbQueryRequest<>(entityManager);
    }

    @Override
    @PostMapping("/")
    public QueryResponse execute(@RequestBody ProductViewQuery productViewQuery) {
        //
        productViewQuery.execute(productViewStore);
        return productViewQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-single")
    public QueryResponse execute(@RequestBody ProductViewDynamicQuery productViewDynamicQuery) {
        //
        productViewDynamicQuery.execute(request);
        return productViewDynamicQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-multi")
    public QueryResponse execute(@RequestBody ProductViewsDynamicQuery productViewsDynamicQuery) {
        //
        productViewsDynamicQuery.execute(request);
        return productViewsDynamicQuery.getQueryResponse();
    }
}
