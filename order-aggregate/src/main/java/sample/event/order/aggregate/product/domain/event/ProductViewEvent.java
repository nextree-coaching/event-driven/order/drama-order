/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.order.aggregate.product.domain.event;

import lombok.Getter;
import lombok.Setter;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEvent;
import sample.event.order.aggregate.product.domain.entity.ProductView;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEventType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
public class ProductViewEvent extends CqrsDataEvent {
    /* Autogen by nara studio */
    private ProductView productView;
    private String productViewId;
    private NameValueList nameValues;

    protected ProductViewEvent(CqrsDataEventType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static ProductViewEvent newProductViewRegisteredEvent(ProductView productView) {
        /* Autogen by nara studio */
        ProductViewEvent event = new ProductViewEvent(CqrsDataEventType.Registered);
        event.setProductView(productView);
        return event;
    }

    public static ProductViewEvent newProductViewModifiedEvent(String productViewId, NameValueList nameValues) {
        /* Autogen by nara studio */
        ProductViewEvent event = new ProductViewEvent(CqrsDataEventType.Modified);
        event.setProductViewId(productViewId);
        event.setNameValues(nameValues);
        return event;
    }

    public static ProductViewEvent newProductViewRemovedEvent(String productViewId) {
        /* Autogen by nara studio */
        ProductViewEvent event = new ProductViewEvent(CqrsDataEventType.Removed);
        event.setProductViewId(productViewId);
        return event;
    }

    public static ProductViewEvent newProductViewRemovedEvent(ProductView productView) {
        /* Autogen by nara studio */
        ProductViewEvent event = new ProductViewEvent(CqrsDataEventType.Removed);
        event.setProductView(productView);
        return event;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static ProductViewEvent fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, ProductViewEvent.class);
    }
}
